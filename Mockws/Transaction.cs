﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mockws.Json.Transaction
{
    public class Transaction
    {
        public string date { get; set; }
        public string type { get; set; }
        public string store_name { get; set; }
        public string store_address { get; set; }
        public string country { get; set; }
        public double amount { get; set; }
        public string icon { get; set; }

        public Transaction(string date, string type, string store_name, string store_address, string country, double amount, string icon)
        {
            this.date = date;
            this.type = type;
            this.store_name = store_name;
            this.store_address = store_address;
            this.country = country;
            this.amount = amount;
            this.icon = icon;
        }

        public Transaction()
        {

        }
    }

    public class RootObject
    {
        public List<Transaction> transactions { get; set; }
    }
}