﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mockws.Json.Location
{
    public class Latlong
    {
        public Latlong(double latitude, double longitude)
        {
            this.lat = latitude;
            this.@long = longitude;
        }
        public double lat { get; set; }
        public double @long { get; set; }

        public override bool Equals(object obj)
        {
            return this.lat == ((Latlong)obj).lat && this.@long == ((Latlong)obj).@long;
        }

        public override int GetHashCode()
        {
            return Convert.ToInt32(this.lat) ^ Convert.ToInt32(this.@long);
        }
    }

    public class RootObject
    {
        public List<Latlong> latlong { get; set; }
    }
}