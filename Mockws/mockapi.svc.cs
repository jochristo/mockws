﻿using Mockws.Json.Location;
using Mockws.Json.Transaction;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace Mockws
{
    
    public class MockApiRS : IMockApiService
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        string IMockApiService.GetData(int value)
        {
            throw new NotImplementedException();
        }

        Stream IMockApiService.latlong(double latitude, double longitude)
        {

            //var lat = Math.Round(latitude, 6);
            //var lon = Math.Round(longitude, 6);



            var i = 10;
            //LocationData locationData = null;
            Json.Location.RootObject rootObject = new Json.Location.RootObject();
            List<Latlong> list = new List<Latlong>();
            HashSet<Latlong> set = new HashSet<Latlong>();

            for (var k = 0; k < i; k++)
            {
                //Latlong item = Distance.getLocation(longitude, latitude, 2000, new Random());
                Latlong item = Distance.getLocation(longitude, latitude, 2000);
                while (set.Contains(item))
                {
                    item = Distance.getLocation(longitude, latitude, 2000);
                }
                set.Add(item);
            }
            rootObject.latlong = set.ToList();            
            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
            {
                StringEscapeHandling = StringEscapeHandling.EscapeHtml
                ,Formatting = Formatting.Indented 

            };
            var json = JsonConvert.SerializeObject(rootObject,Formatting.None, new JsonConverter[] { new StringEnumConverter() }).ToString();
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }


        Stream IMockApiService.GetMockTranscactions(int size)
        {
            var i = size;
            Mockws.Json.Transaction.RootObject rootObject = new Mockws.Json.Transaction.RootObject();
            HashSet<Mockws.Json.Transaction.Transaction> set = new HashSet<Json.Transaction.Transaction>();
            RandomDateTime randomDateTime = new RandomDateTime();
            NumberGenerator numberGenerator = new NumberGenerator();
            CharsGenerator charsGenerator = new CharsGenerator();
            Random random = new Random();
            //DirectoryInfo dir = new DirectoryInfo(Server.MapPath("/allimages"));
            for (var k = 0; k < i; k++)
            {

                Transaction tr = new Transaction();
                tr.date = randomDateTime.NextToString(RandomDateTime.DatetimeType.Date);
                tr.type = RandomTransactions.types[numberGenerator.NextInteger(0, 2)];
                tr.country = RandomTransactions.countries[numberGenerator.NextInteger(0, RandomTransactions.countries.Length)];
                tr.amount = Math.Round(random.NextDouble() * (1500 - 1) + 1,2);
                tr.icon = RandomTransactions.icons[numberGenerator.NextInteger(0,3)];
                tr.store_name = charsGenerator.GetRandomChars(numberGenerator.NextInteger(5, 20));
                tr.store_address = charsGenerator.GetRandomChars(numberGenerator.NextInteger(10, 30));
                set.Add(tr);
            }
            rootObject.transactions = set.ToList();
            var json = JsonConvert.SerializeObject(rootObject, Formatting.None, new JsonConverter[] { new StringEnumConverter() });            
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }

        public Stream GetRandomGeoLocation(double latitude, double longitude)
        {
            var i = 10;
            Json.Location.RootObject rootObject = new Json.Location.RootObject();
            List<Latlong> list = new List<Latlong>();
            HashSet<Latlong> set = new HashSet<Latlong>();
            GeoLocation geoLocation = GeoLocation.FromRadians(latitude, longitude);
            GeoLocation[] bounds = geoLocation.BoundingCoordinates(2000);

            for (var k = 0; k < i; k++)
            {
                //Latlong item = Distance.getLocation(longitude, latitude, 2000, new Random());
                Latlong item = Distance.getLocation(longitude, latitude, 2000);
                while (set.Contains(item))
                {
                    item = Distance.getLocation(longitude, latitude, 2000);
                }
                set.Add(item);
            }
            rootObject.latlong = set.ToList();
            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
            {
                StringEscapeHandling = StringEscapeHandling.EscapeHtml
                ,
                Formatting = Formatting.Indented

            };
            var json = JsonConvert.SerializeObject(rootObject, Formatting.None, new JsonConverter[] { new StringEnumConverter() }).ToString();
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }
    }
}
