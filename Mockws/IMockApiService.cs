﻿using Mockws.Json.Location;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Mockws
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IMockApiService
    {
        [OperationContract]
        string GetData(int value);

        [OperationContract]
        [WebInvoke(Method ="GET", ResponseFormat= WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "/nearby?latitude={latitude}&longtitude={longitude}")]
        Stream latlong(double latitude, double longitude); //?latitude={latitude}&longitude={longitude}

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "/transactions?size={size}")]
        Stream GetMockTranscactions(int size);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "/geoloc?latitude={latitude}&longtitude={longitude}")]
        Stream GetRandomGeoLocation(double latitude, double longitude);

    }
    

    [DataContract]
    public class LocationData
    {
        private double latitude = 0.0;
        private double longitude = 0.0;
        public LocationData() { }

        public LocationData(double latitude, double longitude)
        {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        [DataMember]
        public double Latitude { get => latitude; set => latitude = value; }
        [DataMember]
        public double Longitude { get => longitude; set => longitude = value; }
    }

    [DataContract]
    public class TransactionDataContract
    {
        /*
        date format : dd-mm-yyyy
        type: string values "debit" or "credit"
        store_name: string whatever
        store_address: string whatever
        country: string whatever
        amount: float value
        icon: url of one of test1,test2,test3 attached
        */

        public TransactionDataContract() { }

        private DateTime date = System.DateTime.Now.Date;
        private string type = "credit";
        private string store_name = string.Empty;
        private string store_address = string.Empty;
        private string country = string.Empty;
        private double amount = 0;
        private string icon = string.Empty;

        public DateTime Date { get => date; set => date = value; }
        public string Type { get => type; set => type = value; }
        public string Store_name { get => store_name; set => store_name = value; }
        public string Store_address { get => store_address; set => store_address = value; }
        public string Country { get => country; set => country = value; }
        public double Amount { get => amount; set => amount = value; }
        public string Icon { get => icon; set => icon = value; }
    }

    public static class Distance
    {
        public static double Calculate(double lat1, double long1, double lat2, double long2)
        {
            double R = 6371;
            double dLat = ToRad((lat2 - lat1));
            double dLon = ToRad((long2 - long1));
            lat1 = ToRad(lat1);
            lat2 = ToRad(lat2);
            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            double d = R * c;
            return d;
        }

        public static double ToRad(double value)
        {
            return value * Math.PI / 180;
        }

        public static double distance(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            double theta = lon1 - lon2;
            double dist = Math.Sin(ToRad(lat1)) * Math.Sin(ToRad(lat2)) + Math.Cos(ToRad(lat1)) * Math.Cos(ToRad(lat2)) * Math.Cos(ToRad(theta));
            dist = Math.Acos(dist);
            dist = ToRad(dist);
            dist = dist * 60 * 1.1515;
            if (unit == 'K')
            {
                dist = dist * 1.609344;
            }
            else if (unit == 'N')
            {
                dist = dist * 0.8684;
            }
            return (dist);
        }

        public static Latlong getLocation(double x0, double y0, int radius)
        {
            Random random = new Random();

            // Convert radius from meters to degrees
            double radiusInDegrees = radius / 111000f;

            double u = random.NextDouble();
            double v = random.NextDouble();
            double w = radiusInDegrees * Math.Sqrt(u);
            double t = 2 * Math.PI * v;
            double x = w * Math.Cos(t);
            double y = w * Math.Sin(t);

            // Adjust the x-coordinate for the shrinking of the east-west distances
            //double new_x = x / Math.Cos(ToRad(y0));
            double new_x = x / Math.Cos(y0);

            double foundLongitude = new_x + x0;
            double foundLatitude = y + y0;
            //System.out.println("Longitude: " + foundLongitude + "  Latitude: " + foundLatitude);
            return new Latlong(Math.Round(foundLatitude, 6), Math.Round(foundLongitude,6));
        }

        public static Latlong getLocation(double x0, double y0, int radius, Random random)
        {

            // Convert radius from meters to degrees
            double radiusInDegrees = radius / 111300; // 111000f;

            double u = random.NextDouble();
            double v = random.NextDouble();
            double w = radiusInDegrees * Math.Sqrt(u);
            double t = 2 * Math.PI * v;
            double x = w * Math.Cos(t);
            double y = w * Math.Sin(t);

            // Adjust the x-coordinate for the shrinking of the east-west distances
            double new_x = x / Math.Cos(ToRad(y0));

            double foundLongitude = new_x + x0;
            double foundLatitude = y + y0;
            //System.out.println("Longitude: " + foundLongitude + "  Latitude: " + foundLatitude);
            return new Latlong(Math.Round(foundLatitude, 6), Math.Round(foundLongitude, 6));
        }

        public static Latlong getNearByLocation(double x0, double y0, int radius)
        {
            Random random = new Random();

            // Convert radius from meters to degrees
            double radiusInDegrees = radius / 111300; // 111000f;

            double u = random.NextDouble();
            double v = random.NextDouble();
            double w = radiusInDegrees * Math.Sqrt(u);
            double t = 2 * Math.PI * v;
            double x = w * Math.Cos(t);
            double y = w * Math.Sin(t);

            // Adjust the x-coordinate for the shrinking of the east-west distances
            double new_x = x / Math.Cos(ToRad(y0));

            double foundLongitude = new_x + x0;
            double foundLatitude = y + y0;
            //System.out.println("Longitude: " + foundLongitude + "  Latitude: " + foundLatitude);
            return new Latlong(Math.Round(foundLatitude, 6), Math.Round(foundLongitude, 6));
        }
    }

    public static class RandomTransactions
    {
        public static string[] types = {"debit","credit" };
        public static string[] icons = 
        {
            @"http://10.0.1.134:49998/mockapi/img/test1.png",
            @"http://10.0.1.134:49998/mockapi/img/test2.png",
            @"http://10.0.1.134:49998/mockapi/img/test3.png"
        };

        public static string[] countries =  { "af-ZA","sq-AL","ar-DZ","ar-BH","ar-EG","ar-IQ","ar-JO","ar-KW","ar-LB","ar-LY","ar-MA","ar-OM","ar-QA","ar-SA",
                "ar-SY","ar-TN","ar-AE","ar-YE","hy-AM","Cy-az-AZ","Lt-az-AZ","eu-ES","be-BY","bg-BG","ca-ES","zh-CN","zh-HK","zh-MO","zh-SG","zh-TW","zh-CHS",
                "zh-CHT","hr-HR","cs-CZ","da-DK","div-MV","nl-BE","nl-NL","en-AU","en-BZ","en-CA","en-CB","en-IE","en-JM","en-NZ","en-PH","en-ZA","en-TT",
                "en-GB","en-US","en-ZW","et-EE","fo-FO","fa-IR","fi-FI","fr-BE","fr-CA","fr-FR","fr-LU","fr-MC","fr-CH","gl-ES","ka-GE","de-AT","de-DE","de-LI",
                "de-LU","de-CH","el-GR","gu-IN","he-IL","hi-IN","hu-HU","is-IS","id-ID","it-IT","it-CH","ja-JP","kn-IN","kk-KZ","kok-IN","ko-KR","ky-KZ",
                "lv-LV","lt-LT","mk-MK","ms-BN","ms-MY","mr-IN","mn-MN","nb-NO","nn-NO","pl-PL","pt-BR","pt-PT","pa-IN","ro-RO","ru-RU","sa-IN","Cy-sr-SP",
                "Lt-sr-SP","sk-SK","sl-SI","es-AR","es-BO","es-CL","es-CO","es-CR","es-DO","es-EC","es-SV","es-GT","es-HN","es-MX","es-NI","es-PA","es-PY",
                "es-PE","es-PR","es-ES","es-UY","es-VE","sw-KE","sv-FI","sv-SE","syr-SY","ta-IN","tt-RU","te-IN","th-TH","tr-TR","uk-UA","ur-PK","Cy-uz-UZ",
                "Lt-uz-UZ","vi-VN"};

        public static char[] allchars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._-".ToCharArray();

    }

    /// <summary>
    /// Datetime generator for datetime and date formats.
    /// </summary>
    public class RandomDateTime
    {
        private DateTime start;
        private Random generator = new Random();
        private int range;
        private static string dateTimeFormat = "yyyy-MM-dd HH:mm:ss";
        private static string dateFormat = "dd-MM-yyyy";

        /// <summary>
        /// Initializes a new instance of <see cref="RandomDateTime"/> class with default start date and range values.
        /// </summary>
        public RandomDateTime()
        {
            start = new DateTime(1995, 1, 1);
            range = (DateTime.Today - start).Days;
        }

        private DateTime NextDateTime()
        {
            start = new DateTime(2010, 1, 1);
            range = (DateTime.Today - start).Days;
            return GenererateRandomDate(start.Year, DateTime.Now.Year);
        }

        private DateTime NextDateTime(int year, int month, int day)
        {
            start = new DateTime(year, month, day);
            range = (DateTime.Today - start).Days;
            return GenererateRandomDate(start.Year, DateTime.Now.Year);
        }

        // For date of births  > 18yrs old from today
        private DateTime NextDate()
        {
            this.start = DateTime.Today;
            var high = start.AddYears(-18); // start at 18 yrs old
            var low = high.AddYears(-32); // go as back as 50 yrs max
            range = (high - low).Days;
            return GenererateRandomDate(low.Year, high.Year);
        }

        /// <summary>
        /// Returns a random datetime instance as a string value formatted based on the type given.
        /// </summary>
        /// <param name="datetimeType"></param>
        /// <returns></returns>
        public string NextToString(DatetimeType datetimeType)
        {
            string type = datetimeType == DatetimeType.DateTime ? dateTimeFormat : dateFormat;
            return type = datetimeType == DatetimeType.DateTime ? NextDateTime(2000, 1, 1).ToString(dateTimeFormat) : NextDate().ToString(dateFormat);
        }

        /// <summary>
        /// Represents the type of date-time format to use.
        /// </summary>
        public enum DatetimeType
        {
            DateTime, Date
        }

        /// <summary>
        /// Generates random datetime instance with lower/upper bounds.
        /// </summary>
        /// <param name="lowYear"></param>
        /// <param name="highYear"></param>
        /// <returns></returns>
        public DateTime GenererateRandomDate(int lowYear, int highYear)
        {
            //var random = new Random();
            int year = generator.Next(lowYear, highYear);
            int month = generator.Next(1, 12);
            int day = DateTime.DaysInMonth(year, month);
            int Day = generator.Next(1, day);
            DateTime dt = new DateTime(year, month, Day, 8, 00, 00);
            return dt;
        }
    }

    /// <summary>
    /// Numeric generator with lower/upper bounds.
    /// </summary>
    public class NumberGenerator
    {
        private Random random = new Random();

        public int NextInteger(int low, int high)
        {
            return random.Next(low, high);
        }

        public string NextDouble(int lowerBound, int upperBound, int decimalPlaces)
        {
            if (upperBound <= lowerBound || decimalPlaces < 0)
            {
                throw new ArgumentException("Error occured");
            }
            double value = ((random == null ? new Random() : random).NextDouble() * (upperBound - lowerBound)) + lowerBound;
            string formatted = value.ToString("N7", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            return formatted;
        }
    }

    /// <summary>
    /// Generates random sequences of characters defined in character alphabets of type T.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CharsGenerator
    {
        //private IRandomType iRandomType = new T();
        public string GetRandomChars(int maxSize)
        {
            var chars = RandomTransactions.allchars;
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(maxSize);
            var random = new Random();
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
                if (result.Length == 1)
                {
                    while (result[0] == '0')
                    {
                        result[0] = random.Next(1, 9).ToString().ToCharArray().ElementAt(0);
                    }
                }
            }
            return result.ToString();
        }
    }
}
